import os
import random
from colorama import Fore, Back, Style

jogarNov = "s"
jogadas = 0
quemJoga = 2 # 1=cpu, 2=jogador
maxJogadas = 9
velha = [
    [" ", " ", " "],
    [" ", " ", " "],
    [" ", " ", " "]
]

def tela():

    global velha
    global jogadas

    os.system("cls")

    # melhor resolvido com for
    print(velha[0][0] + "  | " + velha[0][1] + " | "  + velha[0][2])
    print("------------")
    print(velha[1][0] + "  | " + velha[1][1] + " | "  + velha[1][2])
    print("------------")
    print(velha[2][0] + "  | " + velha[2][1] + " | "  + velha[2][2])
    print("jogadas: " + Fore.GREEN + str(jogadas) + Fore.RESET)

def pessoaJoga():

    global jogadas
    global quemJoga
    global maxJogadas

    if quemJoga == 2 and jogadas < maxJogadas: # aqui implementa sua ideia de funcionamento de jogo
        try:
            l = int (input ("linha : "))
            c = int (input ("coluna: "))
            while velha[l][c] != " ": 
                l = int (input ("linha: "))
                c = int (input ("coluna: "))
            velha[l][c] = "X"
            quemJoga = 1
            jogadas += 1
        except:
            print("Jogada invalida")
            os.system("Pause")

def cpuJoga():

    global jogadas
    global quemJoga
    global maxJogadas

    if quemJoga == 1 and jogadas < maxJogadas: # aqui implementa sua ideia de funcionamento de jogo
        l = random.randrange (0,3)
        c = random.randrange (0,3)
        while velha[l][c] != " ":
            l = random.randrange (0,3)
            c = random.randrange (0,3)
        velha[l][c] = "O"
        jogadas += 1
        quemJoga = 2

def verificarVitoria():

    global velha
    vitoria = "n"
    simbolos = ["X", "O"]

    for s in simbolos:
        vitoria = "n"
        # verificar vitoria por linhas
        il = ic = 0
        while il < 3:
            soma = 0
            ic = 0
            while ic < 3:
                if (velha [il][ic] == s):
                   soma += 1
                ic += 1
            if(soma == 3):
                vitoria = s
                break
            il +=1
        if(vitoria != "n"):
            break 

        # verificar vitoria por colunas
        il = ic = 0
        while ic < 3:
            soma = 0
            il = 0
            while il < 3:
                if (velha [il][ic] == s):
                   soma += 1
                il += 1
            if(soma == 3):
                vitoria = s
                break
            ic += 1
        if(vitoria != "n"):
            break

        #verificar vitoria por diagonal 1
        soma = 0 
        idia = 0
        while idia < 3:
            if (velha [idia][idia] == s):
                soma += 1
            idia += 1
        if(soma == 3):
            vitoria = s
            break
        
        #verificar vitoria por diagonal 2
        soma = 0 
        idial = 0
        idiac = 2
        while idiac >= 0:
            if (velha [idial][idiac] == s):
                soma += 1
            idial += 1
            idiac -= 1
        if(soma == 3):
            vitoria = s
            break

    return vitoria

def redefinir():
    global velha
    global jogadas
    global quemJoga
    global maxJogadas
    global vitoria
    
    jogadas = 0
    quemJoga = 2 # 1=cpu, 2=jogador
    maxJogadas = 9
    vitoria = "n"
    velha = [
        [" ", " ", " "],
        [" ", " ", " "],
        [" ", " ", " "]
    ]

while(jogarNov == "s"):
    while True:
        tela()
        pessoaJoga()
        cpuJoga()
        tela()
        vitoria = verificarVitoria()
        if(vitoria != "n") or (jogadas >= maxJogadas):
            break
    print(Fore.RED + "FIM DE JOGO" + Fore.LIGHTYELLOW_EX)
    if(vitoria == "X") or (vitoria == "O"):
        print("Resultado: Jogador " + vitoria + " venceu!")
    else:
        print(" resultado: empate")
    jogarNov = input(Fore.BLUE + "Jogar Novamente? [s/n]  " + Fore.RESET)
    redefinir()