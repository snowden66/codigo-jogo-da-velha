import PySimpleGUI as sg

class TelaJogoDaVelha:

    def __init__(self):
        sg.change_look_and_feel ('LightGreen4')
        # Layout
        layout = [
            [sg.Text('escolha uma posição:', size = (30,1))],
            [sg.Input (size = (5,0), key = 'L1xC1'), sg.Input(size = (5,0), key = 'L1xC2'), sg.Input(size = (5,0), key = 'L1xC3')],
            [sg.Input (size = (5,0), key = 'L2xC1'), sg.Input(size = (5,0), key = 'L2xC2'), sg.Input(size = (5,0), key = 'L2xC3')],
            [sg.Input (size = (5,0), key = 'L3xC1'), sg.Input(size = (5,0), key = 'L3xC2'), sg.Input(size = (5,0), key = 'L3xC3')],
            [sg.Button ('jogar')]
        ]

        # Janela
        self.janela = sg.Window("Jogo da Velha").layout(layout)

    def Iniciar(self):

        while True:
            
            event, values = self.janela.read()
            
            if event == 'jogar':
                print('Botão acionado!')
            # Extrair os dados da tela
            self.button, self.values = self.janela.Read()

            L1xC1 = self.values['L1xC1']
            L1xC2 = self.values['L1xC2']
            L1xC3 = self.values['L1xC3']
            L2xC1 = self.values['L2xC1']
            L2xC2 = self.values['L2xC2']
            L2xC3 = self.values['L2xC3']
            L3xC1 = self.values['L3xC1']
            L3xC2 = self.values['L3xC2']
            L3xC3 = self.values['L3xC3']

            print(f'L1xC1: {L1xC1}', f'L1xC2: {L1xC2}', f'L1xC3: {L1xC3}')
            print(f'L2xC1: {L2xC1}', f'L2xC2: {L2xC2}', f'L2xC3: {L2xC3}')
            print(f'L3xC1: {L3xC1}', f'L3xC2: {L3xC2}', f'L3xC3: {L3xC3}')
            break
           
tela = TelaJogoDaVelha()
tela.Iniciar()
